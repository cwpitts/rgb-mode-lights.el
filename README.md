# rgb-mode-lights
This package leverages [rgb.el](https://gitlab.com/cwpitts/rgb.el) to
set RGB colors based on a major mode.
