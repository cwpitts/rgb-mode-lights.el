;;; rgb-mode-lights.el --- RGB control from Emacs.
;;; Version: 1.0.0
;;; URL: https://gitlab.com/cwpitts/rgb.el
;;; Package-Requires: ((emacs "24.3") (rgb "1.0.0"))

;;; Copyright © 2022 Christopher Pitts <cwpitts@protonmail.com>
;;; Permission is hereby granted, free of charge, to any person
;;; obtaining a copy of this software and associated documentation
;;; files (the “Software”), to deal in the Software without
;;; restriction, including without limitation the rights to use, copy,
;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;; of the Software, and to permit persons to whom the Software is
;;; furnished to do so, subject to the following conditions:
;;; The above copyright notice and this permission notice shall be
;;; included in all copies or substantial portions of the Software.
;;; THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;; DEALINGS IN THE SOFTWARE.

;;; Commentary:
;;; This package uses the OpenRGB software to switch the RGB keyboard
;;; color based on the current major mode.  Depends on the rgb.el
;;; package.

;;; Code:
(require 'rgb)
(require 'cl-lib)
(defvar rgb-mode-lights-device-ids (list))

(defvar rgb-mode-lights-argmap (cl-pairlis '() '()))

(defun rgb-mode-lights-set-rgb-from-mode (mode)
  "Set RGB color based on current major mode MODE."
  (let ((params (assoc-default mode rgb-mode-lights-argmap)))
    (if params
        (mapc (lambda (device-id)
                (rgb-set
                 :device device-id
                 :color (substring (assoc-default 'color params) 1)
                 :mode (assoc-default 'mode params)))
              rgb-mode-lights-device-ids))))

(add-hook 'window-buffer-change-functions
          (lambda (window)
            (rgb-mode-lights-set-rgb-from-mode major-mode)))
(provide 'rgb-mode-lights)
;;; rgb-mode-lights.el ends here
